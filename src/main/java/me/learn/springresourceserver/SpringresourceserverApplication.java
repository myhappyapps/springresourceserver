package me.learn.springresourceserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.security.Principal;
import java.util.Map;

@SpringBootApplication
@EnableMethodSecurity
public class SpringresourceserverApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringresourceserverApplication.class, args);
	}
}

@Service
class GreetingService{
	public Map<String,String> greet(){
		var jwt=(Jwt) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		System.out.println(jwt.getTokenValue());
		return Map.of("message","hello, "+jwt.getClaims());
	}
}

@Controller
@ResponseBody
class GreetingController{
	private final GreetingService greetingService;

    GreetingController(GreetingService greetingService) {
        this.greetingService = greetingService;
    }


	@PreAuthorize("hasRole('ADMIN')")
	@GetMapping("/api/hello")
	Map<String,String> hello(Principal principal){
		System.out.println(principal.getName());
		return this.greetingService.greet();
	}
}


